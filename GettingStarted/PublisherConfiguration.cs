﻿using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace GettingStarted
{
    public class PublisherConfiguration : EntityTypeConfiguration<Publisher>
    {
        public PublisherConfiguration()
        {
            HasMany(p => p.Titles).WithRequired(t => t.Publisher).HasForeignKey(t => t.PublisherId);
        }
    }
}