﻿using System.Data.Entity;
using System.Reflection;

namespace GettingStarted
{
    public class BloggingContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorInfo> AuthorInfos { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<Publisher> Publishers { get; set; }

        public BloggingContext()
            : base("BlogContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}