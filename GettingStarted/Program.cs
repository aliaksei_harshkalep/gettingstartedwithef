﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GettingStarted
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new BloggingContext())
            {
                var authors = ctx.Authors.ToList();

                foreach (var author in authors)
                {
                    Console.WriteLine(author.LastName);
                }

                var first = authors.First();

                first.Titles.Add(new Title
                {
                    Header = "Heelllo",
                    Publisher = ctx.Publishers.First()
                });

                ctx.SaveChanges();
            }
        }
    }
}
