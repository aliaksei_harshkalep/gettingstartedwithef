using System.Collections.Generic;

namespace GettingStarted
{
    public class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual IList<Title> Titles { get; set; }
    }
}