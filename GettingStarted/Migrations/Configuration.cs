using System.Collections.Generic;

namespace GettingStarted.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GettingStarted.BloggingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GettingStarted.BloggingContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var publisher1 = new Publisher
            {
                Name = "Pub #1"
            };

            var title1 = new Title
            {
                Header = "Patterns and practices",
                Publisher = new Publisher
                {
                    Name = "Pub #2"
                }
            };

            context.Authors.AddRange(new[]
            {
                new Author
                {
                    FirstName = "George",
                    LastName = "Martin",
                    Age = 80,
                    AuthorInfo = new AuthorInfo
                    {
                        Biography = "My long biography",
                        Photo = new byte[8]
                    },
                    Titles = new List<Title>
                    {
                        new Title
                        {
                            Header = "Game of thrones",
                            Publisher = publisher1
                        },
                        new Title
                        {
                            Header = "Game of thrones 2",
                            Publisher = publisher1
                        }
                    }
                },
                new Author
                {
                    FirstName = "Wesley",
                    LastName = "Skeen",
                    Age = 60,
                    AuthorInfo = new AuthorInfo
                    {
                        Biography = "My long biography",
                        Photo = new byte[8]
                    },
                    Titles = new List<Title>
                    {
                        title1
                    }
                },
                new Author
                {
                    FirstName = "Jon",
                    LastName = "Skeet",
                    Age = 45,
                    AuthorInfo = new AuthorInfo
                    {
                        Biography = "My long biography",
                        Photo = new byte[8]
                    },
                    Titles = new List<Title>
                    {
                        title1
                    }
                },
            });
        }
    }
}
