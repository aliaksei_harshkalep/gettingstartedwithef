namespace GettingStarted.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuthorInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Photo = c.Binary(),
                        Biography = c.String(),
                        Salary = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Authors", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Age = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Titles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(),
                        PublisherId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Publishers", t => t.PublisherId, cascadeDelete: true)
                .Index(t => t.PublisherId);
            
            CreateTable(
                "dbo.Publishers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AuthorTitles",
                c => new
                    {
                        TitleId = c.Int(nullable: false),
                        Title_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TitleId, t.Title_Id })
                .ForeignKey("dbo.Authors", t => t.TitleId, cascadeDelete: true)
                .ForeignKey("dbo.Titles", t => t.Title_Id, cascadeDelete: true)
                .Index(t => t.TitleId)
                .Index(t => t.Title_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuthorTitles", "Title_Id", "dbo.Titles");
            DropForeignKey("dbo.AuthorTitles", "TitleId", "dbo.Authors");
            DropForeignKey("dbo.Titles", "PublisherId", "dbo.Publishers");
            DropForeignKey("dbo.AuthorInfoes", "Id", "dbo.Authors");
            DropIndex("dbo.AuthorTitles", new[] { "Title_Id" });
            DropIndex("dbo.AuthorTitles", new[] { "TitleId" });
            DropIndex("dbo.Titles", new[] { "PublisherId" });
            DropIndex("dbo.AuthorInfoes", new[] { "Id" });
            DropTable("dbo.AuthorTitles");
            DropTable("dbo.Publishers");
            DropTable("dbo.Titles");
            DropTable("dbo.Authors");
            DropTable("dbo.AuthorInfoes");
        }
    }
}
