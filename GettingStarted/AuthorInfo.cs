﻿using System.ComponentModel.DataAnnotations;

namespace GettingStarted
{
    public class AuthorInfo
    {
        public int Id { get; set; }
        public byte[] Photo { get; set; }
        public string Biography { get; set; }
        public decimal? Salary { get; set; }
        public virtual Author Author { get; set; }
    }
}