﻿using System.Data.Entity.ModelConfiguration;

namespace GettingStarted
{
    public class AuthorConfiguration : EntityTypeConfiguration<Author>
    {
        public AuthorConfiguration()
        {
            HasKey(a => a.Id);
            HasOptional(a => a.AuthorInfo).WithRequired(ai => ai.Author);
            HasMany(a => a.Titles).WithMany(t => t.Authors)
                .Map(cfg => cfg.MapLeftKey("AuthorId").MapLeftKey("TitleId").ToTable("AuthorTitles"));
        }
    }
}