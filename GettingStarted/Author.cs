﻿using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.AccessControl;

namespace GettingStarted
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public virtual IList<Title> Titles { get; set; }
        public virtual AuthorInfo AuthorInfo { get; set; }
    }
}