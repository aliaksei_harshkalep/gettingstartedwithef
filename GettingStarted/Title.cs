﻿using System.Collections.Generic;

namespace GettingStarted
{
    public class Title
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public int PublisherId { get; set; }
        public virtual Publisher Publisher { get; set; }
        public virtual IList<Author> Authors { get; set; }
    }
}